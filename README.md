# DevOps for Mobile / Auto

A prototype GitLab CI template to get started quickly with a DevOps pipeline for iOS. 

Demo: https://youtu.be/cKdxDFCT91I


## Environment Variables

This CI template requires that all of the variables below are set as CI variables in the project (unless otherwise noted as optional).

| Variable                        | Example               | Description                                         |
|---------------------------------|-----------------------|-----------------------------------------------------|
| `APP_IDENTIFIER`                | `com.gitlab.ios-demo` | Xcode project bundle identifier                     |
| `MATCH_APP_IDENTIFIER`          | `com.gitlab.ios-demo` | Same as `APP_IDENTIFIER`
| `APPETIZE_API_TOKEN` (optional) | `tok_abcxyz`          | Appetize API token (if using review apps)           |
| `FASTLANE_ITC_TEAM_ID`          | `1234567890`          | Team ID from iTunes Connect                         |
| `FASTLANE_TEAM_ID`              | `XYZABC123`           | Team ID form the Apple Developer Portal             |
| `FASTLANE_USER`                 | `name@domain.com`     | Email address from Apple Developer Portal           |
| `MACHINE_IMAGE`                 | `macos-12-xcode-13`   | Build machine image ([available images](https://docs.gitlab.com/ee/ci/runners/saas/macos/environment.html#available-images))          |
| `XCODE_PROJECT`                 | `ios_demo.xcodeproj`  | Xcode project name                                  |
| `XCODE_SCHEME`                  | `ios_demo`            | Project build schema                                |

## Predefined Variables

The variables below used but are provided by GitLab CI, so it is not required to set them as CI variables.

| Variable                        | Example                                                             | Description                    |
|---------------------------------|---------------------------------------------------------------------|--------------------------------|
| `GITLAB_PROJECT `               | `gitlab-org/incubation-engineering/devops-for-mobile-apps/ios_demo` | The path of the GitLab project |


## Auto iOS Usage

To use this CI template, set the CI variables listed above, and include the template in the project's `.gitlab-ci.yml` file.

```yaml
include:
  remote: https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/auto/-/raw/main/Auto-iOS/release.gitlab-ci.yml
```

Create the file `fastlane/Fastfile` with the following contents:

```ruby
import_from_git(url: 'https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/auto.git', path: 'Auto-iOS/Fastfile')
```

Also, make sure fastlane is installed

```ruby
source "https://rubygems.org"
gem "fastlane"
```

```shell
bundle install
```

## Documentation

### Fastlane Lanes

The following fastlane lanes are provided. They can be used locally, in a CI job, or can be extended by other custom lanes.

| Command                 | Description                                                               |
|-------------------------|---------------------------------------------------------------------------|
| fastlane test           | Run the application's test suite                                          | 
| fastlane build_unsigned | Create an unsigned build of the app to be used for simulators / emulators |
| fastlane build_signed   | Create a signed build of the app                                          |
| fastlane beta (coming soon) | Create a signed beta release                                          |

### Running Lanes Locally

Any of the above lanes can be used locally with the proper configuration provided. To create a local configuration, create a `.env` file in the root of the project and set the following environment variables:

* `APP_IDENTIFIER`
* `MATCH_APP_IDENTIFIER`
* `FASTLANE_ITC_TEAM_ID`
* `FASTLANE_TEAM_ID`
* `FASTLANE_USER`
* `XCODE_PROJECT`
* `XCODE_SCHEME`
* `GITLAB_PROJECT`
* `MATCH_STORAGE_MODE` (should equal `gitlab_secure_files`)
* `PRIVATE_TOKEN` (a GitLab personal access token)

#### Example local `.env` file

```
FASTLANE_ITC_TEAM_ID = 1234567890
FASTLANE_TEAM_ID = XYZABC123
FASTLANE_USER = name@domain.com
APP_IDENTIFIER = com.gitlab.ios-demo
GITLAB_PROJECT = gitlab-org/incubation-engineering/devops-for-mobile-apps/ios_demo
PRIVATE_TOKEN = glpat-notreal
MATCH_STORAGE_MODE = gitlab_secure_files
MATCH_APP_IDENTIFIER = com.gitlab.ios-demo
```
